<?php

if ($_SERVER['REMOTE_ADDR'] !== 'server ip') {
die("You not authorized to access this page!");
}

set_time_limit(0);

$profileId = 10; // ID of your data profile

require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$profile = Mage::getModel('dataflow/profile');
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
$profile->load($profileId);
if (!$profile->getId()) {
Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
}
Mage::register('current_convert_profile', $profile);
$profile->run();
$recordCount = 0;
$batchModel = Mage::getSingleton('dataflow/batch');
echo "EXPORT COMPLETED. BATCHID: " . $batchModel->getId();
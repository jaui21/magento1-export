<?php

    $root       = '/home/webuser/dev_commercial/';
    $logFile    = 'magento_import.log';

    require_once $root . 'app/Mage.php';

    set_time_limit(0);
    ini_set('memory_limit', '1000M');

    ob_implicit_flush();

    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

    $params             = $argv[1];
    $paramsArray        = json_decode($params, true);
    $batchId            = $paramsArray['batchid'];
    $importIds          = $paramsArray['ids'];

    $saved              = 0;
    $batchModel         = Mage::getModel('dataflow/batch')->load($batchId);
    $batchImportModel   = $batchModel->getBatchImportModel();
    $adapter            = Mage::getModel($batchModel->getAdapter());

    $adapter->setBatchParams($batchModel->getParams());

    foreach ($importIds as $importId) { 

        $batchImportModel->load($importId);

        if (!$batchImportModel->getId()) {

            Mage::log(convert(memory_get_usage()) . " - ERROR: Skip undefined row {$importId}", null, $logFile);
            //echo convert(memory_get_usage()) . " - ERROR: Skip undefined row {$importId}\n";
            continue;

        }

        try {

            $importData = $batchImportModel->getBatchData();
            $adapter->saveRow($importData);

        } catch (Exception $e) {

            Mage::log("Exception : " . $e, null, $logFile);
            //echo "Exception : " . $e;
            continue;

        }

        $saved ++;

    }

    if (method_exists($adapter, 'getEventPrefix')) {

        // Event to process rules relationships after import
        Mage::dispatchEvent($adapter->getEventPrefix() . '_finish_before', array(
        'adapter' => $adapter
        ));

        // Clear affected ids for possible reuse
        $adapter->clearAffectedEntityIds();

    }

    Mage::log("Total Products to Import: " . $saved, null, $logFile);
    echo $saved;

?>
<?php

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=report.csv');						// output headers so that the file is downloaded rather than displayed

require_once '/home/webuser/public_promotions/app/Mage.php';						// load the Magento core
umask(0);

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);

$collection = Mage::getModel('catalog/product')										// load the product collection
 ->getCollection()
 ->addFieldToFilter('hn_website_code', '3-BOT')										// filter the collection if hnc_website_code IS EQUAL TO 3-BOT
 ->addAttributeToSelect('*') 														// select everything from the table
 ->addUrlRewrite(); 																// generate nice URLs

$output = fopen('/home/webuser/public_html/var/import/product-3bot.csv', 'w');  	// create a file pointer connected to the output stream

//fputcsv($output, array('title', 'sku', 'id', 'url'));								// output the column headings
foreach($collection as $product) {
 $categories = $product->getCategoryIds(); 											// load the product categories
 $categoryId = end($categories);													// select the last category in the list
 $category = Mage::getModel('catalog/category')->load($categoryId); 				// load that category
 
 // Collect details in variables
 $sku=$product->getSku();															// get the Product SKU
 $name=$product->getName();															// get the Product Name
 $description=$product->getDescription();											// get the Product Description
 $shortdescription=$product->getShortDescription();									// get the Product Short Description
 $image=$product->getImage();														// get the Product Image Path
 $color=$product->getAttributeText('color');										// get the Product Color Name

 fputcsv($output, array($sku, $name, $description, $shortdescription, $image, $color));  // put the values in csv columns SKU | Name | Descrption | Short Descrption | Image | Color
}
fclose($output);																	// close the file				
?>